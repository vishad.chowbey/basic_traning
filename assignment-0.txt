Assignment-0

1.What is the difference between ‘apt’ and ‘apt-get’?
Ans:There are four main differences between 'apt' and 'apt-get':

 i.The apt tool merges functionalities of apt-get and apt-cache
 ii.Additional output and improved design.
   'api-get' gives only the brief information where as 'api' gives the detailed information
 iii.Changes in command syntax for existing functionalities
 iv.Two new functionalities exclusive to the apt command
   Apart for simplifying existing commands, apt has a couple of its own. They are given below.
   'apt list' and 'apt edit-sources'.
2.What is the difference between ‘apt-get update’ and ‘apt-get upgrade’?
Ans:Apt-get update updates the list of available packages and their versions, but it does not install or upgrade any packages. apt-get upgrade actually installs newer versions of the packages you have. After updating the lists, the package manager knows about available updates for the software you have installed.

3.How to create a new user in the system?
Ans:There are basically two steps:
 i.Log in as root:- By command 'sudo -s' and after that it will ask you for your password.

 ii.Use the command 'useradd aditya' to add the user

4.‘mkdir’ gives an error if the directory is already present, how can you fix it?
Ans:We will use 'mkdir -p fileName' it would not give us an error if the directory already exists and the contents for the directory will not change.

5.What is .bashrc?
Ans:.bashrc file is a script file that's executed when a user logs in. The file itself contains a series of configurations for the terminal session. This includes setting up or enabling: coloring, completion, shell history, command aliases, and more. It is a hidden file and simple ls command won't show the file.

6.How can we list out all the Environment Variables?
Ans:'env' is the command which lists all of the environment variables in the shell.

7.How can we add a new Environment Variable?
Ans:To add an environment variable the export command is used. We give the variable a name, which is what is used to access it in shell scripts and configurations and then a value to hold whatever data is needed in the variable.
To output the value of the environment variable from the shell, we use the echo command and prepend the variable’s name with a dollar ($) sign.
And so long as the variable has a value it will be echoed out. If no value is set then an empty line will be displayed instead.

8.What is shell scripting?
Ans:A shell script is a computer program designed to be run by the Unix shell, a command-line interpreter.
 The various dialects of shell scripts are considered to be scripting languages.
 Typical operations performed by shell scripts include file manipulation,
 program execution, and printing text. A script which sets up the environment,
 runs the program, and does any necessary cleanup, logging, etc. is called a wrapper.