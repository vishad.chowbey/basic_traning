const depclne = require('lodash');  

var obj = {  x: 23 };  

var deepCopy = depclne.cloneDeep(obj);   

console.log('Comparing origianal with'+ ' deep ', obj === deepCopy); 

obj.x = 10; 
console.log('After changing original value'); 
console.log("Original value ", obj); 
console.log("Deep Copy value ", deepCopy); 